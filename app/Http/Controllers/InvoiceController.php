<?php

namespace App\Http\Controllers;

use App\Domain\Invoice;
use App\Services\InvoiceService;
use Illuminate\View\View;


/**
 * Class InvoiceController
 * @package App\Http\Controllers
 */
class InvoiceController extends Controller
{
    /**
     * @var InvoiceService
     */
    protected $invoiceService;

    /**
     * InvoiceController constructor.
     * @param InvoiceService $invoiceService
     */
    public function __construct(InvoiceService $invoiceService)
    {
        $this->invoiceService = $invoiceService;
    }

    /**
     * @return View|null
     */
    public function index(): ?View
    {
        $invoices = $this->invoiceService->getAll();
        return view('invoice/index', ['invoices' => $invoices]);
    }

    /**
     * @param string $id
     * @return View|null
     */
    public function show(string $id): ?View
    {
        /** @var Invoice $invoice */
        $invoice = $this->invoiceService->getById($id);
        return view('invoice/show', ['invoice' => $invoice]);
    }

    /**
     * @param string $id
     * @return string
     */
    public function approve(string $id)
    {
        try {
            $this->invoiceService->approve($this->invoiceService->getById($id));
        } catch (\Exception $e) {
            return $e->getMessage();
        }
        return response()->json(['status' => 200]);

    }

    /**
     * @param string $id
     * @return string
     */
    public function reject(string $id)
    {
        try {
            $this->invoiceService->reject($this->invoiceService->getById($id));
        } catch (\Exception $e) {
            return $e->getMessage();
        }
        return response()->json(['status' => 200]);
    }
}
