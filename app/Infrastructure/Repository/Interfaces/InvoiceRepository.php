<?php

namespace App\Infrastructure\Repository\Interfaces;

use App\Domain\Invoice;
use Illuminate\Database\Eloquent\Collection;

/**
 * Interface InvoiceRepository
 * @package App\Infrastructure\Repository\Interfaces
 */
interface InvoiceRepository
{
    /**
     * @param string $id
     * @return Invoice|null
     */
    public function getById(string $id): ?Invoice;

    /**
     * @return Collection|null
     */
    public function getAll(): ?Collection;

    /**
     * @param Invoice $invoice
     * @return Invoice|null
     */
    public function update(Invoice $invoice): ?Invoice;
}