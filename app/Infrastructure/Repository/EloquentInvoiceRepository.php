<?php

namespace App\Infrastructure\Repository;

use App\Domain\Invoice;
use App\Infrastructure\Repository\Interfaces\InvoiceRepository;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class EloquentInvoiceRepository
 * @package App\Infrastructure\Repository
 */
class EloquentInvoiceRepository implements InvoiceRepository
{
    /**
     * @param string $id
     * @return Invoice|null
     */
    public function getById(string $id): ?Invoice
    {
        return Invoice::with('products')->find(['id' => $id])->first();
    }

    /**
     * @return Collection|null
     */
    public function getAll(): ?Collection
    {
        return Invoice::all();
    }

    /**
     * @param Invoice $invoice
     * @return Invoice|null
     */
    public function update(Invoice $invoice): ?Invoice
    {
        $invoice->save();
        return $invoice;
    }
}