<?php

namespace App\Providers;

use App\Infrastructure\Repository\EloquentInvoiceRepository;
use App\Infrastructure\Repository\Interfaces\InvoiceRepository;
use Illuminate\Support\ServiceProvider;

class InvoiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(InvoiceRepository::class, EloquentInvoiceRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
