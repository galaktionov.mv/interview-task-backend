<?php

namespace App\Services;

use App\Domain\Enums\StatusEnum;
use App\Domain\Invoice;
use App\Infrastructure\Repository\Interfaces\InvoiceRepository;
use App\Modules\Approval\Api\Dto\ApprovalDto;
use App\Modules\Approval\Application\ApprovalFacade;
use Illuminate\Database\Eloquent\Collection;
use Ramsey\Uuid\Uuid;

/**
 * Class InvoiceService
 * @package App\Services
 */
class InvoiceService
{
    protected $invoiceRepository;
    protected $approvalService;

    /**
     * InvoiceService constructor.
     * @param InvoiceRepository $invoiceRepository
     */
    public function __construct(InvoiceRepository $invoiceRepository, ApprovalFacade $approvalFacade)
    {
        $this->approvalService = $approvalFacade;
        $this->invoiceRepository = $invoiceRepository;
    }

    /**
     * @param string $id
     * @return Invoice|null
     */
    public function getById(string $id): ?Invoice
    {
        return $this->invoiceRepository->getById($id);
    }

    /**
     * @return Collection|null
     */
    public function getAll(): ?Collection
    {
        return $this->invoiceRepository->getAll();
    }

    /**
     * @param Invoice $invoice
     * @return Invoice|null
     */
    public function approve(Invoice $invoice): ?Invoice
    {
        $approved = $this->approvalService->approve($this->prepareDto($invoice));
        if ($approved) {
            $invoice->status = StatusEnum::APPROVED;
            $this->invoiceRepository->update($invoice);
        }
        return $invoice;
    }

    /**
     * @param Invoice $invoice
     * @return Invoice|null
     */
    public function reject(Invoice $invoice): ?Invoice
    {
        $rejected = $this->approvalService->reject($this->prepareDto($invoice));
        if ($rejected) {
            $invoice->status = StatusEnum::REJECTED;
            $this->invoiceRepository->update($invoice);
        }
        return $invoice;
    }

    /**
     * @param Invoice $invoice
     * @return ApprovalDto|null
     */
    private function prepareDto(Invoice $invoice): ?ApprovalDto
    {
        return new ApprovalDto(
            Uuid::getFactory()->fromString($invoice->id),
            $invoice->status,
            $invoice->toJson()
        );
    }
}