<?php

namespace App\Domain;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use App\Domain\Enums\StatusEnum;
use Illuminate\Support\Facades\Date;

/**
 * Class Invoice
 * @property string $id
 * @property string $number
 * @property Date $date
 * @property Date $due_date
 * @property int $company_id
 * @property StatusEnum $status
 * @property int $created_at
 * @property int $updated_at
 *
 * @package App\Domain
 */
class Invoice extends Model
{
    use HasFactory;

    protected $table = 'invoices';

    public $incrementing = false;

    /**
     * @param $value
     * @return StatusEnum
     */
    public function getStatusAttribute($value): StatusEnum
    {
        return StatusEnum::from($value);
    }

    /**
     * Add totals attribute for Invoice to represent sum of all products
     * @return mixed
     */
    public function getTotalsAttribute()
    {
        return $this->products->sum(function ($product) {
            return $product->price * $product->pivot->quantity;
        });
    }

    /**
     * @return BelongsToMany
     */
    public function Products(): BelongsToMany
    {
        return $this->belongsToMany(Product::class, 'invoice_product_lines')->withPivot('quantity');
    }

    /**
     * @return HasOne
     */
    public function Company(): HasOne
    {
        return $this->hasOne(Company::class, 'id', 'company_id');
    }
}
