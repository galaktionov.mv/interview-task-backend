@extends('common.scaffold')

@section('title')
    {{ $invoice->number }}
@endsection

@section('content')
    <table class="table table-bordered">
        <tbody>
        <tr>
            <th>Invoice ID</th>
            <td>{{ $invoice->id }}</td>
        </tr>
        <tr>
            <th>Invoice Number</th>
            <td>{{ $invoice->number }}</td>
        </tr>
        <tr>
            <th>Invoice Status</th>
            <td>{{ $invoice->status }}</td>
        </tr>
        <tr>
            <th>Invoice Company</th>
            <td>{{ $invoice->company->name }}</td>
        </tr>
        <tr>
            <th>Invoice Date</th>
            <td>{{ $invoice->date }}</td>
        </tr>
        <tr>
            <th>Invoice Due Date</th>
            <td>{{ $invoice->due_date }}</td>
        </tr>
        </tbody>
    </table>
    @if(count($invoice->products)>0)
        <table class="table table-bordered table-striped">
            <tbody>
            <tr>
                <th>Product name</th>
                <th>Product price</th>
                <th>Product quantity</th>
            </tr>
            @foreach($invoice->products as $product)
                <tr>
                    <td>{{ $product->name }}</td>
                    <td>${{ $product->price }}</td>
                    <td>{{ $product->pivot->quantity }}</td>
                </tr>
            @endforeach
            </tbody>
            <tfoot>
                <tr>
                    <th colspan="2">Total</th>
                    <td>${{ $invoice->totals }}</td>
                </tr>
            </tfoot>
        </table>
    @endif
    @if ($invoice->status == App\Domain\Enums\StatusEnum::DRAFT)
    <a href="{{ route('invoice.approve', ['id' => $invoice->id]) }}" class="btn btn-success" id="approveButton">Approve</a>
    <a href="{{ route('invoice.reject', ['id' => $invoice->id]) }}" class="btn btn-danger" id="rejectButton">Reject</a>
    @endif
    <a href="{{ route('invoice.index') }}" class="btn btn-primary">Back</a>
@endsection

@section('scripts')
    <script>

        function sendRequest(type, id) {
            url = ['', 'invoices', id, type].join('/');
            fetch(url, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'X-CSRF-TOKEN': '{{ csrf_token() }}' // Add CSRF token if needed
                },
                body: JSON.stringify({}) // Add any payload data if needed
            })
                .then(function(response) {
                    // Check the response status
                    if (response.ok) {
                        // Success response, display an alert or refresh the page
                        alert('Invoice status updated');
                        location.reload(); // Refresh the page
                    } else {
                        // Error response, display an alert or handle the error
                        alert('Invoice status change failed');
                    }
                })
                .catch(function(error) {
                    // Handle any network or other errors
                    console.error('Error:', error);
                });
        }
        // Attach a click event listener to the button
        document.getElementById('approveButton').addEventListener('click', function() {
            sendRequest('approve', '{{ $invoice->id }}');
        });
        document.getElementById('rejectButton').addEventListener('click', function () {
            sendRequest('reject', '{{ $invoice->id }}');
        });
    </script>
@endsection

