@extends('common.scaffold')

@section('title')
    Invoices list
@endsection

@section('content')
    <table class="table table-bordered table-striped">
        <thead>
        <tr>
            <th>ID</th>
            <th>Invoice Number</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        @foreach($invoices as $invoice)
            <tr>
                <td>{{ $invoice->id }}</td>
                <td>{{ $invoice->number }}</td>
                <td><a href='{{ route('invoice.view', ['id' => $invoice->id]) }}' class="btn btn-primary">View</a></td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection


