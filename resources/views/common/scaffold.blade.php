<html>
<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</head>
<div class="scaffold">
    <div class="container">
        <div class="card">
            <div class="card-body">
                <h2>@yield('title')</h2>

                <div class="content">
                    @yield('content')
                </div>
            </div>
        </div>
    </div>

</div>
@yield('scripts')
</html>